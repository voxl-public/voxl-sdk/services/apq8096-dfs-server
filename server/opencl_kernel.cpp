/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "opencl_kernel.h"

#include <iostream>

OpenCLKernel::OpenCLKernel(OpenCLManager& opencl_manager_in, const std::string& kernel_name_in):
    opencl_manager(opencl_manager_in),
    kernel_name(kernel_name_in)
{
}

OpenCLKernel::~OpenCLKernel()
{

}


void OpenCLKernel::compileKernel(const std::string& kernel_code, const std::string& compiler_options)
{
    // Create the source (this is the source code) of the OpenCL kernel
    cl::Program::Sources source(1, std::make_pair(kernel_code.c_str(), kernel_code.size()));

    // Create a program from the source
    cl::Program program = cl::Program(this->opencl_manager.getContext(), source);

    try
    {
        // Compile the program!
        program.build(this->opencl_manager.getDevices(), compiler_options.c_str());

        // Create the kernel from the compiled code.  Also this is where we select the actual kernel
        this->kernel = cl::Kernel(program, this->kernel_name.c_str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        if ((error.err() == CL_BUILD_PROGRAM_FAILURE) || (error.err() == CL_INVALID_BINARY))
        {
            std::string build_log;
            program.getBuildInfo(this->opencl_manager.getDevices()[0], CL_PROGRAM_BUILD_LOG, &build_log);
            error_msg += " ";
            error_msg += build_log;
        }

        throw std::runtime_error(error_msg);
    }
}


int OpenCLKernel::roundUp(int value, int multiple)
{
    int remainder = value % multiple;
    if (remainder != 0)
    {
        value += multiple - remainder;
    }

    return value;
}

void OpenCLKernel::setWorkGroupsAndOffset(cl::NDRange local_group_size_in, cl::NDRange global_group_size_in, cl::NDRange launch_offset_in)
{
    this->local_group_size = local_group_size_in;
    this->global_group_size = global_group_size_in;
    this->launch_offset = launch_offset_in;
}

cl::Kernel& OpenCLKernel::getKernel()
{
    return this->kernel;
}

void OpenCLKernel::launchKernel()
{
    // Launch the kernel with the appropriate work groups!
    this->opencl_manager.getCommandQueue().enqueueNDRangeKernel(this->kernel, this->launch_offset, this->global_group_size, this->local_group_size);
}   