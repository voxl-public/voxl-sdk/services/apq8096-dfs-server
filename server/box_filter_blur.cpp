/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "box_filter_blur.h"
// #include "oclUtils.h"

// C/C++ Includes
#include <string>
#include <sstream>
#include <iostream>

// OpenCV includes
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// The Kernel variables we will be using to load the opencl blur kernel code
extern "C" const char blur_kernel_code[];
extern "C" const size_t blur_kernel_code_len;

BoxFilterBlur::BoxFilterBlur(OpenCLManager& opencl_manager_in, int image_width_in, int image_height_in,  int filter_size_in):
    opencl_manager(opencl_manager_in),
    horizontal_kernel(opencl_manager_in, "boxFilterBlurHorizontalPass"),
    vertical_kernel(opencl_manager_in, "boxFilterBlurVerticalPass"),
    image_width(image_width_in),
    image_height(image_height_in),
    filter_size(filter_size_in)
{
    // Make sure the filter size is an odd number otherwise this whole thing wont work
    if ((this->filter_size % 2) != 1)
    {
        std::string error_msg = "Filter size for the BoxFilterBlur must be an odd number.";
        throw std::runtime_error(error_msg);
    }

    /** Create the 2 image buffers that we need.  We are assuming the images are 8 bit and thus the total
     *  for each image is just Width x Height
     */
    this->memory_size = static_cast<int>(this->image_width) * static_cast<int>(this->image_height);
    this->in_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_ONLY, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->horizontal_pass_out_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);
    this->vertical_pass_out_img_buffer_image2d = cl::Image2D (this->opencl_manager.getContext(), CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_UNSIGNED_INT8), this->image_width, this->image_height, 0, nullptr, nullptr);

    // Compute some things about the filter
    this->filter_radius = static_cast<int>(this->filter_size / 2);

    // Setup the kernels
    this->initHorizontalKernel();
    this->initVerticalKernel();
}

BoxFilterBlur::~BoxFilterBlur()
{

}
void BoxFilterBlur::initHorizontalKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = HORIZONTAL_KERNEL_LOCAL_WORK_GROUP_X;
        int ly = HORIZONTAL_KERNEL_LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(1, lx);
        int gy = OpenCLKernel::roundUp(this->image_height, ly);
        this->horizontal_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Compute the norm factor
        float norm_factor =  (filter_radius * 2) + 1;
        norm_factor = 1.0f / norm_factor;

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DSIZE="          << this->memory_size;
        options << " -DFILTER_RADIUS=" << this->filter_radius;
        options << " -DNORM_FACTOR="   << norm_factor;

        // Compile the kernel!
        std::string kernel_code(blur_kernel_code);
        this->horizontal_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void BoxFilterBlur::initVerticalKernel()
{
    try
    {
        // Create the local and global sizes and set them
        int lx = VERTICAL_KERNEL_LOCAL_WORK_GROUP_X;
        int ly = VERTICAL_KERNEL_LOCAL_WORK_GROUP_Y;
        int gx = OpenCLKernel::roundUp(this->image_width, lx);
        int gy = OpenCLKernel::roundUp(1, ly);
        this->vertical_kernel.setWorkGroupsAndOffset(cl::NDRange(lx, ly), cl::NDRange(gx, gy), cl::NDRange(0, 0));

        // Compute the norm factor
        float norm_factor =  (filter_radius * 2) + 1;
        norm_factor = 1.0f / norm_factor;

        // Create the compile options for this kernel
        std::stringstream options("");
        options << " -DWIDTH="         << this->image_width;
        options << " -DHEIGHT="        << this->image_height;
        options << " -DSIZE="          << this->memory_size;
        options << " -DFILTER_RADIUS=" << this->filter_radius;
        options << " -DNORM_FACTOR="   << norm_factor;

        // Compile the kernel!
        std::string kernel_code(blur_kernel_code);
        this->vertical_kernel.compileKernel(kernel_code, options.str());
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}


void BoxFilterBlur::compute(cv::Mat& input)
{
    try
    {
        // // this->opencl_manager.getCommandQueue().enqueueWriteBuffer(this->in_img_buffer, CL_FALSE, 0, this->memory_size, input.data);

        cl::size_t<3> origin;
        origin[0] = 0;
        origin[1] = 0;
        origin[2] = 0;

        cl::size_t<3> region;
        region[0] = this->image_width;
        region[1] = this->image_height;
        region[2] = 1;

        // Move the memory from the CPU to the GPU in a non blocking way
        this->opencl_manager.getCommandQueue().enqueueWriteImage(in_img_buffer_image2d, CL_FALSE, origin, region, 0, 0, input.data);

        // Fill in the horizontal kernel arguments
        int i = 0;
        this->horizontal_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(in_img_buffer_image2d));
        this->horizontal_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(horizontal_pass_out_img_buffer_image2d));

        // Fill in the vertical kernel arguments
        i = 0;
        this->vertical_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(horizontal_pass_out_img_buffer_image2d));
        this->vertical_kernel.getKernel().setArg(i++, sizeof(cl_mem), &(vertical_pass_out_img_buffer_image2d));

        // Launch the kernels
        this->horizontal_kernel.launchKernel();
        this->vertical_kernel.launchKernel();
    }
    catch (cl::Error error)
    {
        std::string error_msg = "Had OpenCL error: ";
        error_msg += error.what();
        throw std::runtime_error(error_msg);
    }
}

void BoxFilterBlur::getResults(cv::Mat& output)
{
    cl::size_t<3> origin;
    origin[0] = 0;
    origin[1] = 0;
    origin[2] = 0;

    cl::size_t<3> region;
    region[0] = this->image_width;
    region[1] = this->image_height;
    region[2] = 1;

    this->opencl_manager.getCommandQueue().enqueueReadImage(vertical_pass_out_img_buffer_image2d, CL_TRUE, origin, region, 0, 0, output.data);
}


cl::Image2D& BoxFilterBlur::getOutputBuffer()
{
    return this->vertical_pass_out_img_buffer_image2d;
}
