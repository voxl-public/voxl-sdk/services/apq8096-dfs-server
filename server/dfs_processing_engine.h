/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODALAI_DFS_PROCESSING_ENGINE_H
#define MODALAI_DFS_PROCESSING_ENGINE_H

// Project Includes
#include "opencl_manager.h"
#include "box_filter_blur.h"
#include "sobel_filter.h"
#include "sad_disparity_estimation.h"

// ModalAI includes
#include <modal_pipe_server.h>
#include <modal_pipe_client.h>

// OpenCV Includes
#include <opencv2/core/mat.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/stereo.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>

// C/C++ Includes
#include <atomic>
#include <thread>
#include <queue>

class DFSProcessingEngine
{
public:

	/** The data products that are computed by this engine
	 */
	struct DataProducts
	{
		/** The meta information for the camera frames
		 */
		camera_image_metadata_t meta;

		/** The left image
		 */
		cv::Mat left_image;

		/** The raw disparity image
		 */
		cv::Mat raw_left_disparity;

		/** The raw disparity image
		 */
		cv::Mat raw_right_disparity;

		/** The filtered disparity image
		 */
		cv::Mat disparity_filtered;

		/** The scaled disparity, scaled to 0-255
		 */
		cv::Mat disparity_scaled;

		/** The Point cloud
		 */
		cv::Mat point_cloud_xyz;
	};

	/** The callback that we will issue when data products are ready
	 */
	typedef void (*DataProductReadyCallback)(DataProducts &data_product);


	/** Constructor
	 *
	 *  @param image_width_in The input image width
	 *  @param image_height_in The input image height
	 *  @param rescale_factor_in The rescale factor to use (aka the image is scaled to be 1.0/rescale_factor_in of its original size)
	 *  @param is_debug_mode_in If debug mode is enabled
	 *  @param do_save_images_in If we should save images for debug
	 *  @param debug_image_save_location_in The location to save the images to
	 */
	DFSProcessingEngine(uint16_t image_width_in,
						uint16_t image_height_in,
						uint16_t rescale_factor_in,
						bool is_debug_mode_in,
						bool is_timing_mode_in,
						bool do_save_images_in,
						std::string debug_image_save_location_in,
						uint32_t number_of_disparity_levels_in);

	/** Destructor, makes sure the system terminates (aka kills threads)
	 */
	~DFSProcessingEngine();

	/** Init the processing engine (read configs, setup opencl kernels, ext)
	 *
	 *  @param instrinsic_calib_filepath The filepath to the intrinsic calibration file
	 *  @param extrinsic_calib_filepath The filepath to the extrinsic calibration file
	 *  @return true if all was successful
	 */
	bool init(std::string instrinsic_calib_filepath, std::string extrinsic_calib_filepath);

	/** Register the callback to use when data is ready
	 *
	 *  @param callback the callback to use when data is ready
	 */
	void register_data_products_ready_callback(DataProductReadyCallback callback);

	/** Start the processing engine
	 *  Does nothing if the engine is already running
	 */
	void start();

	/** Stops the processing engine
	 *  Does nothing if the engine is already stopped
	 */
	void stop();

	/** Check if the processing engine is already running
	 *
	 *  @return true if the processing engine already started
	 */
	bool is_started();

	/** Called to input camera data into the processing engine
	 *
	 *  @param meta The metadata of the new frame
	 *  @param frame_data The data for the left and right image frames as read from the modalai pipe system
	 */
	int add_new_camera_data(const camera_image_metadata_t &meta, char* frame_data);

private:

	/** Constants
	 */
	//static constexpr uint32_t REMAPPED_DOWNSIZED_IMAGE_SLEEP_USEC = 1000;
	//static constexpr uint32_t COMPUTE_DISPARITY_SLEEP_USEC = 1000;
	static constexpr uint32_t POST_PROCESSING_SLEEP_USEC = 2000;
	static constexpr uint32_t CENSUS_FILTER_SIZE = 16;

	/** Tunable parameters
	 */
	static constexpr uint32_t BOX_FILTER_BLUR_FILTER_SIZE = 7;
	//static constexpr uint32_t STARTUP_FRAME_SKIP_COUNTER = 15;
	//static constexpr uint32_t MAX_QUEUE_SIZE = 10;
	static constexpr double WSL_LAMBDA = 8000.0;
	static constexpr double WSL_SIGMA = 1.5;
	static constexpr double WSL_DDR = 0.33;

	/** Structure that holds the camera data
	 */
	struct CameraData
	{
		/** The meta information for the camera frames
		*/
		camera_image_metadata_t meta;

		/** The right image
		 */
		cv::Mat right_image;

		/** The left image
		 */
		cv::Mat left_image;
	};


	/** Load the calibration files
	 *
	 *  @param instrinsic_calib_filepath The filepath to the intrinsic calibration file
	 *  @param extrinsic_calib_filepath The filepath to the extrinsic calibration file
	 *  @return true if all was successful
	 */
	bool load_calib(std::string instrinsic_calib_filepath, std::string extrinsic_calib_filepath);


	/** Save an image to the file save location if file saving is enabled
	 *  Does nothing if file saving is not enabled
	 *
	 *  @param img The image to save
	 *  @param filename The name of the file t save in "debug_image_save_location"
	 */
	void save_image(cv::Mat& img, const std::string& filename);

	/** The threaded worker function for downsizing and remapping the image
	 */
	//void remap_downsize_worker();

	/** The threaded worker function for computing the disparity map
	 */
	//void compute_disparity_worker();

	/** The threaded worker function for doing the post processing of the disparity map
	 */
	void disparity_post_processing_worker();

	/** The image dimensions
	 */
	uint16_t image_width{0};
	uint16_t image_height{0};

	/** The size of the output
	 */
	uint16_t output_image_width{0};
	uint16_t output_image_height{0};


	/** The frame rate of the incoming data
	 */
	uint16_t incoming_data_fps{0};

	/** The rescale factor for the image
	 */
	uint16_t rescale_factor{0};

	/** Some flags that tell us the current state debug state of the system
	 */
	bool is_debug_mode{false};
	bool en_timing{false};
	bool do_save_images{false};

	/** The location that we should save debug images to
	 */
	std::string debug_image_save_location{""};

	uint32_t number_of_disparity_levels{24};

	/** The number of skips that we have remaining
	 */
	//uint32_t skips_remaining{0};

	/** The maps that we will use for remapping the images
	 */
	cv::Mat map11;
	cv::Mat map12;
	cv::Mat map21;
	cv::Mat map22;

	/** The WSL Filter for post processing
	 */
	std::shared_ptr<cv::ximgproc::DisparityWLSFilter> wls_filter;

	/** The flag for if we should continue running or not
	 */
	std::atomic<bool> keep_running;

	/** The various threads that we will be using
	 */
	// std::thread remap_downsize_thread;
	// std::thread compute_disparity_thread;
	std::thread disparity_post_processing_thread;

	/** The various queues for passing things between threads
	 */
	std::queue<std::shared_ptr<CameraData>> raw_image_queue;
	std::queue<std::shared_ptr<CameraData>> remapped_downsized_image_queue;
	std::queue<std::shared_ptr<DataProducts>> disparity_image_queue;

	/** The mutexs we will be using for the queues
	 */
	std::mutex raw_image_queue_mutex;
	std::mutex remapped_downsized_image_queue_mutex;
	std::mutex disparity_image_queue_mutex;

	/** The opencl manager, the think managing the opencl context
	 */
	OpenCLManager opencl_manager;

	/** The box filter computation engines
	 */
	std::shared_ptr<BoxFilterBlur> left_blur;
	std::shared_ptr<BoxFilterBlur> right_blur;

	/** The sobel filters we will be using
	 */
	std::shared_ptr<SobelFilter> left_sobel;
	std::shared_ptr<SobelFilter> right_sobel;

	/** The sad disparity estimator
	 */
	std::shared_ptr<SADDisparityEstimator> sad_disparity_estimator;

	/** The callback to use when data products are ready
	 */
	DataProductReadyCallback data_product_ready_callback{nullptr};

	/** The 4x4 perspective transformation matrix for 3D re-projection
	 */
	cv::Mat Q;
	cv::Mat Q_scaled;
	// scale for point cloud downsizing only!
	int pc_scale = 8;
};

#endif //MODALAI_DFS_PROCESSING_ENGINE_H
