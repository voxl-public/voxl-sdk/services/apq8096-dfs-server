/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// The sampler we will be using for accessing the image buffers
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;



__kernel void sobelFilter(read_only image2d_t input,
                          write_only image2d_t output,
                          const int cutoff)

{

    // Get the various IDs for this thread
    const int2 gid = (int2)(get_global_id(0), get_global_id(1));
    const int2 lid = (int2)(get_local_id(0), get_local_id(1));

    // Local memory we will load the data into
    __local uchar local_memory_in[LOCAL_SIZE];

    // Private variables we will be using
    int index;
    short x;
    short y;
    short x_g;
    short y_g;


    // Load from global to local memory
    for (x = lid.x; x < (LOCAL_WORK_SIZE_X + 2); x += LOCAL_WORK_SIZE_X)
    {
        for (y = lid.y; y < (LOCAL_WORK_SIZE_Y + 2); y += LOCAL_WORK_SIZE_Y)
        {
        	y_g = (gid.y + y - lid.y - 1);
        	x_g = (gid.x + x - lid.x - 1);

        	// Make sure nothing is out of bounds
            if ((x_g >= WIDTH) || (x_g < 0) || (y_g >= HEIGHT) || (y_g < 0))
            {
                continue;
            }

            local_memory_in[y * (LOCAL_WORK_SIZE_X + 2) + x] = read_imagei(input, sampler, (int2)(x_g, y_g)).x;
        }
    }

    // Wait till all threads loaded the data into the memory
    barrier(CLK_LOCAL_MEM_FENCE);

    // Skip all the ones that are not on valid rows
    if ((gid.x >= WIDTH - 1) || (gid.y >= HEIGHT - 1) || (gid.x < 1) || (gid.y < 1))
    {
        // @todo deal with borders
	    // write_imagei(output, gid, 0);
        return;
    }

    /************************************************************************************************************************
    ** Note: We reuse the local variables so that we can reduce the number of registers we are using overall int he kernel
    ************************************************************************************************************************/

    // Compute the sobel filter for the X derivatives
    index = (lid.y) * (LOCAL_WORK_SIZE_X + 2);
    x = local_memory_in[ index + (lid.x + 2)];
    x -= local_memory_in[ index + (lid.x)];

    index = (lid.y + 1) * (LOCAL_WORK_SIZE_X + 2);
    x += 2 * local_memory_in[index + (lid.x + 2)];
    x -= 2 * local_memory_in[index + (lid.x)];

    index = (lid.y + 2) * (LOCAL_WORK_SIZE_X + 2);
    x += local_memory_in[ index + (lid.x + 2)];
    x -= local_memory_in[ index + (lid.x)];

    // Compute the sobel filter for the Y derivatives
    index = (lid.y) * (LOCAL_WORK_SIZE_X + 2);
    y = -local_memory_in[ index + (lid.x)];
    y -= 2 * local_memory_in[ index + (lid.x + 1)];
    y -= local_memory_in[ index + (lid.x + 2)];

    index = (lid.y + 2) * (LOCAL_WORK_SIZE_X + 2);
    y += local_memory_in[ index + (lid.x)];
    y += 2 * local_memory_in[ index + (lid.x + 1)];
    y += local_memory_in[ index + (lid.x + 2)];

    // Put it all together as a weighted combination
    //write_imagei(output, gid, 0.5*abs(x) + 0.5*abs(y));

    int tmp = 0.5*abs(x) + 0.5*abs(y);
    if (tmp < cutoff)
    {
        tmp = 0;
    }
    // Put it all together as a weighted combination
    write_imagei(output, gid, tmp);

    // Kept here for debug purposes.  This basically takes the input and puts it onto the output
    // output[gid.y * WIDTH + gid.x] = local_memory_in[(lid.y + 1) * (LOCAL_WORK_SIZE_X + 2) + (lid.x + 1)];


    return;
}