/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "dfs_processing_engine.h"
#include "undistort.h"
#include "constants.h"
#include "config_file.h"
#include <modal_pipe_server.h>
#include <modal_pipe_client.h>

// C/C++ Includes
#include <iostream>
#include <mutex>
#include <unistd.h>
#include <chrono>


undistort_map_t undistort_map_l;
undistort_map_t undistort_map_r;

static int64_t _apps_time_monotonic_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


DFSProcessingEngine::DFSProcessingEngine(uint16_t image_width_in,
		uint16_t image_height_in,
		uint16_t rescale_factor_in,
		bool is_debug_mode_in,
		bool en_timing_in,
		bool do_save_images_in,
		std::string debug_image_save_location_in,
		uint32_t number_of_disparity_levels_in):
	image_width{image_width_in},
	image_height{image_height_in},
	rescale_factor{rescale_factor_in},
	is_debug_mode{is_debug_mode_in},
	en_timing{en_timing_in},
	do_save_images{do_save_images_in},
	debug_image_save_location{debug_image_save_location_in},
	number_of_disparity_levels{number_of_disparity_levels_in},
	keep_running{false}
{


}

DFSProcessingEngine::~DFSProcessingEngine()
{
	// Stop the threads if they are running
	this->stop();
}

bool DFSProcessingEngine::init(std::string instrinsic_calib_filepath, std::string extrinsic_calib_filepath)
{
	// Load the calibration files
	if (!this->load_calib(instrinsic_calib_filepath, extrinsic_calib_filepath))
	{
		return false;
	}

	// Compute the size of the final remapped image that we will compute depth on
	this->output_image_width = this->image_width / this->rescale_factor;
	this->output_image_height = this->image_height / this->rescale_factor;

	try
	{
		// Setup the box filters
		uint32_t filter_size = BOX_FILTER_BLUR_FILTER_SIZE;
		this->left_blur = std::shared_ptr<BoxFilterBlur>(new BoxFilterBlur(this->opencl_manager, output_image_width, output_image_height, filter_size));
		this->right_blur = std::make_shared<BoxFilterBlur>(this->opencl_manager, output_image_width, output_image_height, filter_size);

		// Setup the sobel filters
		this->left_sobel = std::shared_ptr<SobelFilter>(new SobelFilter(this->opencl_manager, output_image_width, output_image_height));
		this->right_sobel = std::shared_ptr<SobelFilter>(new SobelFilter(this->opencl_manager, output_image_width, output_image_height));

		// Create the disperity estimator
		//uint32_t number_of_disperity_levels = NUMBER_OF_DISPARITY_LEVELS;
		this->sad_disparity_estimator = std::shared_ptr<SADDisparityEstimator>(new SADDisparityEstimator(this->opencl_manager, output_image_width, output_image_height, this->number_of_disparity_levels));
	}
	catch (std::runtime_error& e)
	{
		// Oh Boy!!! Something went very wrong!! :(
		std::cerr << e.what() << std::endl;
		return false;
	}

	// Setup the WSL filter
	this->wls_filter = cv::ximgproc::createDisparityWLSFilterGeneric(false);
	this->wls_filter->setLambda(WSL_LAMBDA);
	this->wls_filter->setSigmaColor(WSL_SIGMA);
	this->wls_filter->setDepthDiscontinuityRadius(ceil(WSL_DDR * static_cast<double>(CENSUS_FILTER_SIZE)));

	return true;
}

void DFSProcessingEngine::register_data_products_ready_callback(DataProductReadyCallback callback)
{
	this->data_product_ready_callback = callback;
}


void DFSProcessingEngine::start()
{
	// Only start if we have not already started
	if (this->keep_running == true)
	{
		return;
	}

	// Set that we should keep running
	this->keep_running = true;

	// Set the number of skips since we are starting up
	//this->skips_remaining = STARTUP_FRAME_SKIP_COUNTER;

	// Start the threads
	// this->remap_downsize_thread = std::thread(&DFSProcessingEngine::remap_downsize_worker, this);
	// this->compute_disparity_thread = std::thread(&DFSProcessingEngine::compute_disparity_worker, this);
	this->disparity_post_processing_thread = std::thread(&DFSProcessingEngine::disparity_post_processing_worker, this);
}

void DFSProcessingEngine::stop()
{
	// Only stop if we are not already stopped
	if (this->keep_running == false)
	{
		return;
	}

	// Set that we should stop running
	this->keep_running = false;

	// Join the threads
	// if (this->remap_downsize_thread.joinable())
	// {
	// 	this->remap_downsize_thread.join();
	// }

	// if (this->compute_disparity_thread.joinable())
	// {
	// 	this->compute_disparity_thread.join();
	// }

	if (this->disparity_post_processing_thread.joinable())
	{
		this->disparity_post_processing_thread.join();
	}
}

bool DFSProcessingEngine::is_started()
{
	return this->keep_running;
}

int DFSProcessingEngine::add_new_camera_data(const camera_image_metadata_t &meta, char* frame_data)
{
	// if (this->skips_remaining > 0)
	// {
	// 	this->skips_remaining--;
	// }

	// // Do nothing if there is no one to consume the data products
	// if (!this->is_debug_mode && (this->data_product_ready_callback == nullptr))
	// {
	// 	return -1;
	// }

	// // getting backed up, report this as a skip.
	// if(this->raw_image_queue.size() > 0){
	// 	return 1;
	// }

	// Check if the image width and heights match what we expect
	if ((meta.width != this->image_width) || (meta.height != this->image_height))
	{
		std::cerr << "ERROR: Got stereo image with invalid size.  Expected size: (";
		std::cerr << this->image_width << ", " << this->image_height << ")  Got:(" << meta.width << ", " << meta.height << ")" << std::endl;
		return -1;
	}

	int64_t start_time = _apps_time_monotonic_ns();

	// Create the camera data
	std::shared_ptr<CameraData> rectified_camera_data = std::make_shared<CameraData>();

	// Compute the size of each image frame
	int frame_size = this->image_width * this->image_height * sizeof(uint8_t);

	// Fill in the camera image data
	std::memcpy(&(rectified_camera_data->meta), &meta, sizeof(camera_image_metadata_t));

	// rectify the images, putting new data into cv mats
	rectified_camera_data->left_image = cv::Mat(this->image_height, this->image_width, CV_8UC1);
	rectified_camera_data->right_image = cv::Mat(this->image_height, this->image_width, CV_8UC1);
	mcv_undistort_image((uint8_t*)frame_data, rectified_camera_data->left_image.data, &undistort_map_l);
	mcv_undistort_image((uint8_t*)(frame_data+frame_size), rectified_camera_data->right_image.data, &undistort_map_r);

	// only in timing mode, print how long it took
	if(this->en_timing){
		int64_t process_time = _apps_time_monotonic_ns() - start_time;
		double t_ms = ((double)process_time)/1000000.0;
		static double avg_ms = 7.0;
		avg_ms = (0.95*avg_ms)+(0.05*t_ms);
		printf("remap took        %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
	}


////////////////////////////////////////////////////////////////////////////////
// GPU stuff now, this is the slowest chunk of computation
////////////////////////////////////////////////////////////////////////////////

	start_time = _apps_time_monotonic_ns();

	// // Blur the images
	// this->left_blur->compute(rectified_camera_data->left_image);
	// this->right_blur->compute(rectified_camera_data->right_image);

	// // Compute the sobel of the images
    // this->left_sobel->compute(this->left_blur->getOutputBuffer());
    // this->right_sobel->compute(this->right_blur->getOutputBuffer());
    this->left_sobel->compute(rectified_camera_data->left_image);
    this->right_sobel->compute(rectified_camera_data->right_image);

	// Compute the disparity
	this->sad_disparity_estimator->compute(this->left_sobel->getOutputBuffer(), this->right_sobel->getOutputBuffer());

	// Make the data products, dont forget the meta!
	std::shared_ptr<DataProducts> results = std::make_shared<DataProducts>();
	results->meta = rectified_camera_data->meta;
	results->left_image = rectified_camera_data->left_image;

	// Get the results
	results->raw_left_disparity = cv::Mat(this->output_image_height, this->output_image_width, CV_8UC1);
	results->raw_right_disparity = cv::Mat(this->output_image_height, this->output_image_width, CV_8UC1);
	this->sad_disparity_estimator->getResults(results->raw_left_disparity, results->raw_right_disparity);
	this->save_image(results->raw_left_disparity, "disparity_raw.jpg");

	// only in timing mode, print how long it took
	if(this->en_timing){
		int64_t process_time = _apps_time_monotonic_ns() - start_time;
		double t_ms = ((double)process_time)/1000000.0;
		static double avg_ms = 2.0;
		avg_ms = (0.95*avg_ms)+(0.05*t_ms);
		printf("\ngpu stuff took    %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
	}

	// Push it into the correct queue
	{
		std::lock_guard<std::mutex> lock(this->disparity_image_queue_mutex);

		// Keep the queue size bound
		while (this->disparity_image_queue.size() > 1)
		{
			if (this->is_debug_mode)
			{
				std::cout << "Popping disparity_image_queue: " << this->disparity_image_queue.size() << std::endl;
			}

			this->disparity_image_queue.pop();
		}

		// if (this->is_debug_mode)
		// {
		// 	std::cout << "disparity_image_queue size: " << this->disparity_image_queue.size() << std::endl;
		// }

		// Add to the queue
		this->disparity_image_queue.push(results);
	}


	return 0;
}

bool DFSProcessingEngine::load_calib(std::string instrinsic_calib_filepath, std::string extrinsic_calib_filepath)
{
	// Read intrinsic parameters
	cv::FileStorage fs(instrinsic_calib_filepath, cv::FileStorage::READ);
	if (!fs.isOpened())
	{
		std::cerr << "Failed to open calibration file: " << instrinsic_calib_filepath << std::endl;
		return false;
	}


	// Read the calib data into the matrices
	cv::Mat M1;
	cv::Mat D1;
	cv::Mat M2;
	cv::Mat D2;
	fs["M1"] >> M1;
	fs["D1"] >> D1;
	fs["M2"] >> M2;
	fs["D2"] >> D2;

	// Rescale the calib data to match the image size that we will be remapping
	M1 /= this->rescale_factor;
	M2 /= this->rescale_factor;

	int w_scaled = this->image_width / this->rescale_factor;
	int h_scaled = this->image_height / this->rescale_factor;
	cv::Size img_size(w_scaled, h_scaled);

	// Open extrinsics file and generate rectification maps
	fs.open(extrinsic_calib_filepath, cv::FileStorage::READ);
	if (!fs.isOpened())
	{
		std::cerr << "Failed to open calibration file: " << extrinsic_calib_filepath << std::endl;
		return false;
	}

	// Read the calibration data into the matrices
	cv::Mat R;
	cv::Mat T;
	fs["R"] >> R;
	fs["T"] >> T;

	// Compute the remap maps
	cv::Mat R1;
	cv::Mat P1;
	cv::Mat R2;
	cv::Mat P2;
	cv::stereoRectify( M1, D1, M2, D2, img_size, R, T, R1, R2, P1, P2, this->Q, cv::CALIB_ZERO_DISPARITY, -1, img_size);
	// make sure this uses CV_32_FC2 map format so we can read it and convert to our own
	// MCV undistortion map format
	cv::initUndistortRectifyMap(M1, D1, R1, P1, img_size, CV_32FC2, this->map11, this->map12);
	cv::initUndistortRectifyMap(M2, D2, R2, P2, img_size, CV_32FC2, this->map21, this->map22);

	// generate new projection matrices for the downscaled image for point cloud
	cv::stereoRectify( M1/pc_scale, D1, M2/pc_scale, D2, img_size/pc_scale, R, T, R1, R2, P1, P2, this->Q_scaled, cv::CALIB_ZERO_DISPARITY, -1, img_size/pc_scale);

	// do our modalai cv bilinear undistort
	mcv_init_undistort_map_from_cvmat(this->image_width, this->image_height, &undistort_map_l, (float*)this->map11.data);
	mcv_init_undistort_map_from_cvmat(this->image_width, this->image_height, &undistort_map_r, (float*)this->map21.data);
	// printf("map1: ch %d rows %d cols %d\n", this->map11.channels(), this->map11.rows, this->map11.cols);
	// printf("map2: ch %d rows %d cols %d\n", this->map12.channels(), this->map12.rows, this->map12.cols);

	return true;
}


void DFSProcessingEngine::save_image(cv::Mat& img, const std::string& filename)
{
	// If image saving is not enabled then do nothing
	if (!this->do_save_images)
	{
		return;
	}

	// Save it to the correct directory (overwrites files with the same name that are already there)
	std::string filepath = this->debug_image_save_location + "/" + filename;
	cv::imwrite(filepath, img);

	if (this->is_debug_mode)
	{
		std::cout << "Saving image: " << filepath << std::endl;
	}
}

/*
void DFSProcessingEngine::remap_downsize_worker()
{
	// Loop until told to stop looping
	while (this->keep_running)
	{
		// Read frames we will be processing
		std::shared_ptr<CameraData> raw_camera_data{nullptr};
		{
			std::lock_guard<std::mutex> lock(this->raw_image_queue_mutex);
			if (!this->raw_image_queue.empty())
			{
				// If there is data get it and remove it from the queue
				raw_camera_data = this->raw_image_queue.front();
				this->raw_image_queue.pop();
			}

		}

		// If we didnt get a new frame then sleep for a bit and move on
		if (raw_camera_data == nullptr)
		{
			usleep(REMAPPED_DOWNSIZED_IMAGE_SLEEP_USEC);
			continue;
		}

		// cv::medianBlur(raw_camera_data->left_image, raw_camera_data->left_image, 5);
		// cv::medianBlur(raw_camera_data->right_image, raw_camera_data->right_image, 5);

		// Save the raw versions of the images
		this->save_image(raw_camera_data->left_image, "left_raw.jpg");
		this->save_image(raw_camera_data->right_image, "right_raw.jpg");

		// Process the camera data
		std::shared_ptr<CameraData> rectified_camera_data = std::make_shared<CameraData>();
		rectified_camera_data->meta = raw_camera_data->meta;

		// Rescale if needed.  Note: This is rescaling in place
		if (this->rescale_factor != 1)
		{
			int method = cv::INTER_LINEAR; // other options: INTER_AREA INTER_CUBIC
			float opencv_resize_factor = 1.0f / static_cast<float>(this->rescale_factor);
			cv::resize(raw_camera_data->left_image, raw_camera_data->left_image, cv::Size(), opencv_resize_factor, opencv_resize_factor, method);
			cv::resize(raw_camera_data->right_image, raw_camera_data->right_image, cv::Size(), opencv_resize_factor, opencv_resize_factor, method);

			// Save the Downsized versions of the images
			this->save_image(raw_camera_data->left_image, "left_downsized.jpg");
			this->save_image(raw_camera_data->right_image, "right_downsized.jpg");
		}



		// remap the camera frame
		//cv::remap(raw_camera_data->left_image, rectified_camera_data->left_image, this->map11, this->map12, cv::INTER_LINEAR);
		//cv::remap(raw_camera_data->right_image, rectified_camera_data->right_image, this->map21, this->map22, cv::INTER_LINEAR);
		rectified_camera_data->left_image = cv::Mat::zeros(480,640,CV_8UC1);
		rectified_camera_data->right_image = cv::Mat::zeros(480,640,CV_8UC1);

		int64_t start_time = _apps_time_monotonic_ns();
		mcv_undistort_image(raw_camera_data->left_image.data, rectified_camera_data->left_image.data, &undistort_map_l);
		mcv_undistort_image(raw_camera_data->right_image.data, rectified_camera_data->right_image.data, &undistort_map_r);

		// only in timing mode, print how long it took
		if(this->en_timing){
			int64_t process_time = _apps_time_monotonic_ns() - start_time;
			double t_ms = ((double)process_time)/1000000.0;
			static double avg_ms = 7.0;
			avg_ms = (0.95*avg_ms)+(0.05*t_ms);
			printf("remap took        %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
		}

		// Save the remapped versions of the images
		this->save_image(rectified_camera_data->left_image, "left_rectified.jpg");
		this->save_image(rectified_camera_data->right_image, "right_rectified.jpg");

		// Push it into the correct queue
		std::lock_guard<std::mutex> lock(this->remapped_downsized_image_queue_mutex);

		// Keep the queue size bound
		while (this->remapped_downsized_image_queue.size() > MAX_QUEUE_SIZE)
		{
			if (this->is_debug_mode)
			{
				std::cout << "Popping remapped_downsized_image_queue: " << this->remapped_downsized_image_queue.size() << std::endl;
			}

			this->remapped_downsized_image_queue.pop();
		}

		if (this->is_debug_mode)
		{
			std::cout << "remapped_downsized_image_queue size: " << this->remapped_downsized_image_queue.size() << std::endl;
		}

		// Add to the queue
		this->remapped_downsized_image_queue.push(rectified_camera_data);

	}
}
*/

/*
void DFSProcessingEngine::compute_disparity_worker()
{
	// Loop until told to stop looping
	while (this->keep_running)
	{
		// Read frames we will be processing
		std::shared_ptr<CameraData> camera_data{nullptr};
		{
			std::lock_guard<std::mutex> lock(this->remapped_downsized_image_queue_mutex);
			if (!this->remapped_downsized_image_queue.empty())
			{
				// If there is data get it and remove it from the queue
				camera_data = this->remapped_downsized_image_queue.front();
				this->remapped_downsized_image_queue.pop();
			}
		}

		// If we didnt get a new frame then sleep for a bit and move on
		if (camera_data == nullptr)
		{
			usleep(COMPUTE_DISPARITY_SLEEP_USEC);
			continue;
		}

		int64_t start_time = _apps_time_monotonic_ns();

		// Blur the images
		this->left_blur->compute(camera_data->left_image);
		this->right_blur->compute(camera_data->right_image);

		// Compute the sobel of the images
		this->left_sobel->compute(this->left_blur->getOutputBuffer());
		this->right_sobel->compute(this->right_blur->getOutputBuffer());

		// Compute the disparity
		this->sad_disparity_estimator->compute(this->left_sobel->getOutputBuffer(), this->right_sobel->getOutputBuffer());

		// Make the data products, dont forget the meta!
		std::shared_ptr<DataProducts> results = std::make_shared<DataProducts>();
		results->meta = camera_data->meta;
		results->left_image = camera_data->left_image;

		// Get the results
		results->raw_left_disparity = cv::Mat(this->output_image_height, this->output_image_width, CV_8UC1);
		results->raw_right_disparity = cv::Mat(this->output_image_height, this->output_image_width, CV_8UC1);
		this->sad_disparity_estimator->getResults(results->raw_left_disparity, results->raw_right_disparity);
		this->save_image(results->raw_left_disparity, "disparity_raw.jpg");

		// only in timing mode, print how long it took
		if(this->en_timing){
			int64_t process_time = _apps_time_monotonic_ns() - start_time;
			double t_ms = ((double)process_time)/1000000.0;
			static double avg_ms = 2.0;
			avg_ms = (0.95*avg_ms)+(0.05*t_ms);
			printf("gpu stuff took    %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
		}

		// Push it into the correct queue
		{
			std::lock_guard<std::mutex> lock(this->disparity_image_queue_mutex);

			// Keep the queue size bound
			while (this->disparity_image_queue.size() > MAX_QUEUE_SIZE)
			{
				if (this->is_debug_mode)
				{
					std::cout << "Popping disparity_image_queue: " << this->disparity_image_queue.size() << std::endl;
				}

				this->disparity_image_queue.pop();
			}

			if (this->is_debug_mode)
			{
				std::cout << "disparity_image_queue size: " << this->disparity_image_queue.size() << std::endl;
			}


			// Add to the queue
			this->disparity_image_queue.push(results);
		}
	}
}

*/

void DFSProcessingEngine::disparity_post_processing_worker()
{
	// // Some variables for out FPS calculation
	// auto fps_timer_start = std::chrono::high_resolution_clock::now();
	// uint32_t fps_frames_processed = 0;

	// Loop until told to stop looping
	while (this->keep_running)
	{

		// auto fps_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - fps_timer_start);
		// if (fps_duration.count() >= 1000)
		// {
		// 	//float fps = (static_cast<float>(fps_frames_processed) / static_cast<float>(fps_duration.count())) * 1000.0f;
		// 	//std::cout << "Data Products FPS: " << fps << std::endl;

		// 	// Restart the Calculation
		// 	fps_frames_processed = 0;
		// 	fps_timer_start = std::chrono::high_resolution_clock::now();
		// }

		// Read frames we will be processing
		std::shared_ptr<DataProducts> data_products{nullptr};
		{
			std::lock_guard<std::mutex> lock(this->disparity_image_queue_mutex);
			if (!this->disparity_image_queue.empty())
			{
				// If there is data get it and remove it from the queue
				data_products = this->disparity_image_queue.front();
				this->disparity_image_queue.pop();
			}
		}

		// If we didnt get a new frame then sleep for a bit and move on
		if (data_products == nullptr)
		{
			usleep(POST_PROCESSING_SLEEP_USEC);
			continue;
		}


////////////////////////////////////////////////////////////////////////////////
// now do post-processing, starting with median blur to remove the speckles
// always do this since we only got to this point if we have clients and
// all 3 output pipes require this step
////////////////////////////////////////////////////////////////////////////////

		int64_t start_time = _apps_time_monotonic_ns();

		cv::medianBlur(data_products->raw_left_disparity, data_products->disparity_filtered, first_median_filter_size);
		if(en_second_median_filter){
			cv::medianBlur(data_products->disparity_filtered, data_products->disparity_filtered, second_median_filter_size);
		}

		// only in timing mode, print how long it took
		if(this->en_timing){
			int64_t process_time = _apps_time_monotonic_ns() - start_time;
			double t_ms = ((double)process_time)/1000000.0;
			static double avg_ms = 2.0;
			avg_ms = (0.95*avg_ms)+(0.05*t_ms);
			printf("median blur took  %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
		}

		camera_image_metadata_t disp_meta = data_products->meta;
		disp_meta.format = IMAGE_FORMAT_RAW8;
		disp_meta.height = data_products->disparity_filtered.rows;
		disp_meta.width = data_products->disparity_filtered.cols;
		disp_meta.size_bytes = disp_meta.height * disp_meta.width;
		disp_meta.stride = disp_meta.width;
		disp_meta.timestamp_ns = data_products->meta.timestamp_ns;
		pipe_server_send_camera_frame_to_channel(DISPARITY_PIPE_CH, disp_meta, (char*)data_products->disparity_filtered.data);


////////////////////////////////////////////////////////////////////////////////
// only calculate and output to the scaled pipe if there is a client or
// we are in a debug mode. This is just a preview pipe, not usually on.
////////////////////////////////////////////////////////////////////////////////

		if(this->en_timing || this->is_debug_mode	|| \
						pipe_server_get_num_clients(DISPARITY_SCALED_PIPE_CH)>0){

			start_time = _apps_time_monotonic_ns();

			// convert to 8-bit
			// Scale the disparity (useful for human viewing)
			data_products->disparity_filtered.convertTo(data_products->disparity_scaled, CV_8UC1, 255.0f/ static_cast<float>(this->number_of_disparity_levels));

			// only in timing mode, print how long it took
			if(this->en_timing){
				int64_t process_time = _apps_time_monotonic_ns() - start_time;
				double t_ms = ((double)process_time)/1000000.0;
				static double avg_ms = 2.0;
				avg_ms = (0.95*avg_ms)+(0.05*t_ms);
				printf("scaling took      %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
			}

			disp_meta = data_products->meta;
			disp_meta.format = IMAGE_FORMAT_RAW8;
			disp_meta.height = data_products->disparity_scaled.rows;
			disp_meta.width = data_products->disparity_scaled.cols;
			disp_meta.size_bytes = disp_meta.height * disp_meta.width;
			disp_meta.stride = disp_meta.width;
			disp_meta.timestamp_ns = data_products->meta.timestamp_ns;
			pipe_server_send_camera_frame_to_channel(DISPARITY_SCALED_PIPE_CH, disp_meta, (char*)data_products->disparity_scaled.data);
		}


////////////////////////////////////////////////////////////////////////////////
// only calculate and output to the point cloud pipe if there is a client or
// we are in a debug mode.
////////////////////////////////////////////////////////////////////////////////

		if(this->en_timing || this->is_debug_mode	|| \
						pipe_server_get_num_clients(POINT_CLOUD_PIPE_CH)>0){

			start_time = _apps_time_monotonic_ns();
			// don't need lots of points, so downscale image
			cv::Mat disparity_ptcloud_scale;
			cv::resize    (data_products->disparity_filtered,  disparity_ptcloud_scale, cv::Size(),1.0/pc_scale, 1.0/pc_scale, cv::INTER_NEAREST);


			// Compute the point cloud
			cv::Mat disp_float;
			disparity_ptcloud_scale.convertTo(disp_float, CV_32F, 1.0f/pc_scale);
			cv::reprojectImageTo3D(disp_float, data_products->point_cloud_xyz, this->Q_scaled, true);

			// only in timing mode, print how long it took
			if(this->en_timing){
				int64_t process_time = _apps_time_monotonic_ns() - start_time;
				double t_ms = ((double)process_time)/1000000.0;
				static double avg_ms = 2.0;
				avg_ms = (0.95*avg_ms)+(0.05*t_ms);
				printf("point cloud took  %6.2fms avg: %6.2fms\n", t_ms, avg_ms);
			}

			point_cloud_metadata_t pc_meta;
			pc_meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;
			pc_meta.timestamp_ns = data_products->meta.timestamp_ns;
			pc_meta.n_points = data_products->point_cloud_xyz.rows * data_products->point_cloud_xyz.cols;
			pc_meta.format = 0;
			pipe_server_send_point_cloud_to_channel(POINT_CLOUD_PIPE_CH, pc_meta, (float*)data_products->point_cloud_xyz.data);
		}

		// // We processed a frame!
		// fps_frames_processed++;

		// // Issue the callback!
		// if (this->data_product_ready_callback)
		// {
		// 	data_product_ready_callback(*data_products);
		// }

	} // end of while loop

	return;
}
