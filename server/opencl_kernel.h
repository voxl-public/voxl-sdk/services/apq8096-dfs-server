/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef OPENCL_KERNEL_h
#define OPENCL_KERNEL_h

// Project Includes
#include "opencl_manager.h"

// OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// C/C++ Includes
#include <vector>
#include <memory>

/** Class that will handle the opencl device/context stuff.
 */
class OpenCLKernel
{
public:

    /** Constructor
     *
     *  @param opencl_manager_in The OpenCLManager object to use
     *  @param kernel_name_in The name of the kernel to load from the kernel code
     *  @throws std::runtime_error if the kernel could not be created or compiled or something
     */
    OpenCLKernel(OpenCLManager& opencl_manager_in, const std::string& kernel_name_in);

    /** Destructor
     */
    ~OpenCLKernel();

    /** Compile the opencl kernel code into the kernel object
     *
     *  @param kernel_code The kernel code to compile
     *  @param compiler_options The options to pass into the compiler
     *  @throws std::runtime_error if there was a compile time issue
     */
    void compileKernel(const std::string& kernel_code, const std::string& compiler_options);

    /** Set the workgroup sizes/offset for this kernel
     *
     *  @param local_group_size_in The local work group size
     *  @param global_group_size_in The global work group size
     *  @param launch_offset_in The launch offset to use (usually set to (0,0,0))
     */
    void setWorkGroupsAndOffset(cl::NDRange local_group_size_in, cl::NDRange global_group_size_in, cl::NDRange launch_offset_in);

    /** Gets the raw opencl Kernel object
     *
     *  @returns The raw opencl kernel object
     */
    cl::Kernel& getKernel();

    /** Launch the opencl Kernel
     */
    void launchKernel();


    /** Helper function that rounds the value up so that it is a multiple of "multiple".
     *  This is useful for computing global work groups from local work groups.
     *
     *  @param value The value to round up
     *  @param multiple The multiple that the round up value must evenly divide (aka rounded_up_value%multiple==0)
     *  @return the rounded up value
     */
    static int roundUp(int value, int multiple);

protected:

    /** The OpenCL manager that created the opencl context we want to use
     */
    OpenCLManager& opencl_manager;

    /** The name of the kernel
     */
    std::string kernel_name;

    /** The actual Kernel we will be using
     */
    cl::Kernel kernel;

    /** The local and global work groups that will be used when launching the kernels
     */
    cl::NDRange local_group_size;
    cl::NDRange global_group_size;

    /** The launch offset which is always (0,0)
     */
    cl::NDRange launch_offset;
};

#endif // OPENCL_KERNEL_h
