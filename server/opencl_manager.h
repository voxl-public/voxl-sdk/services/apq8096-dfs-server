/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef OPENCL_MANAGER_h
#define OPENCL_MANAGER_h

// OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// OpenCV Includes
#include <opencv2/core/core.hpp>

// C/C++ Includes
#include <vector>
#include <memory>

/** Class that will handle an opencl kernel
 */
class OpenCLManager
{
public:

    /** Constructor
     *
     *  @throws std::runtime_error if the OpenCL context cannot be created
     */
    OpenCLManager();

    /** Destructor
     */
    ~OpenCLManager();

    /** Get the OpenCL context that this manager created
     *
     *  @return the OpenCL context
     */
    cl::Context& getContext();

    /** Get the OpenCL command queue that this manager created
     *
     *  @return the OpenCL command queue
     */
    cl::CommandQueue& getCommandQueue();

    /** Get the OpenCL devices that this manager detected
     *
     *  @return the OpenCL devices that were detected
     */
    std::vector<cl::Device>& getDevices();

    /** Blocks until the queue managed by this instance is finished running
     */
    void finishQueue();

    /** Convert a cv::Mat to an opencl cl::Image2D.  This copies the data to the GPU
     *
     *  @param input The cv::Mat to convert
     *  @result the cl::Image2D created
     */
    cl::Image2D convertMatToClImage2D(cv::Mat input);

private:

    /** The OpenCL context that this manager creates
     */
    cl::Context cl_context;

    /** The OpenCL command queue that we will schedule things on
     */
    cl::CommandQueue command_queue;

    /** A lost of the opencl devices that are present in the system.  This should only be 1 on VOXL platforms
     */
    std::vector<cl::Device> opencl_devices;
};

#endif // OPENCL_MANAGER_h
