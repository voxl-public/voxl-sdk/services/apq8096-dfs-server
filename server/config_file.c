/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include "config_file.h"
#include <modal_json.h>

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration specific to voxl-dfs-server.\n\
 *\n\
 * skip_n_frames\n\
 *                      how many frames to skip between processed frames. For 30hz\n\
 *                      input frame rate, we recommend skipping 5 frames resulting\n\
 *                      in 5hz disparity and depth data.\n\
 *\n\
 * n_disparity_levels\n\
 *                      number of disaprity levels to check, must be positive\n\
 *                      but doesn't need to be a multiple of 16 like opencv.\n\
 *                      For VGA resolution 48 works up to less than a meter away\n\
 *                      but is a bit slow. 24 is faster but only works at longer\n\
 *                      distances.\n\
 *\n\
 * sobel_cutoff\n\
 *                      regions of the image with gradients below this value will\n\
 *                      be ignored. Increase this to reduce false-positives, but\n\
 *                      doing so will also reduce the amount of depth reported\n\
 *                      across the image. 5-10 is a reasonable range.\n\
 *\n\
 * first_median_filter_size\n\
 *                      must be an odd number. This filter serves to remove\n\
 *                      speckles and noise after calculating pixel disparity.\n\
 *\n\
 * en_second_median_filter & second_median_filter_size\n\
 *                      The second median filter can be smaller and serves\n\
 *                      to clean up the edges of objects as well as remove\n\
 *                      any remaining speckles the first filter left\n\
 */\n"


int skip_n_frames;
int n_disparity_levels;
int sobel_cutoff;
int first_median_filter_size;
int en_second_median_filter;
int second_median_filter_size;

void config_file_print(void)
{
	printf("=================================================================\n");
	printf("skip_n_frames:                    %d\n",    skip_n_frames);
	printf("n_disparity_levels:               %d\n",    n_disparity_levels);
	printf("sobel_cutoff:                     %d\n",    sobel_cutoff);
	printf("first_median_filter_size:         %d\n",    first_median_filter_size);
	printf("en_second_median_filter:          %d\n",    en_second_median_filter);
	printf("second_median_filter_size:        %d\n",    second_median_filter_size);
	printf("=================================================================\n");
	return;
}


int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONFIG_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONFIG_FILE);

	cJSON* parent = json_read_file(CONFIG_FILE);
	if(parent==NULL) return -1;

	// actually parse values
	json_fetch_int_with_default(parent, "skip_n_frames",			&skip_n_frames,				5);
	json_fetch_int_with_default(parent, "n_disparity_levels",		&n_disparity_levels,		48);
	json_fetch_int_with_default(parent, "sobel_cutoff",				&sobel_cutoff,				5);
	json_fetch_int_with_default(parent, "first_median_filter_size",	&first_median_filter_size,	17);
	json_fetch_bool_with_default(parent, "en_second_median_filter", &en_second_median_filter,	1);
	json_fetch_int_with_default(parent, "second_median_filter_size",&second_median_filter_size,	7);

	if(sobel_cutoff<1){
		fprintf(stderr, "ERROR parsing config file, sobel_cutoff must be >=1\n");
		return -1;
	}
	if(first_median_filter_size<3){
		fprintf(stderr, "ERROR parsing config file, first_median_filter_size must be >=3\n");
		return -1;
	}
	if(!(first_median_filter_size%2)){
		fprintf(stderr, "ERROR parsing config file, first_median_filter_size must be odd\n");
		return -1;
	}
	if(second_median_filter_size<3){
		fprintf(stderr, "ERROR parsing config file, second_median_filter_size must be >=3\n");
		return -1;
	}
	if(!(second_median_filter_size%2)){
		fprintf(stderr, "ERROR parsing config file, second_median_filter_size must be odd\n");
		return -1;
	}

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONFIG_FILE);
		cJSON_Delete(parent);
		return -1;
	}


	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		//printf("The config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONFIG_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}



