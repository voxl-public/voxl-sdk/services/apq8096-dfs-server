/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef BOX_FILTER_BLUR_H
#define BOX_FILTER_BLUR_H

// Project Includes
#include "opencl_kernel.h"
#include "opencl_manager.h"

// OpenCL Includes
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

// OpenCV Includes
#include <opencv2/core/core.hpp>


class BoxFilterBlur
{
public:

	/** Constructor
	 *  Sets everything up for computing this operation (opencl kernel/buffers/ext)
	 *
	 *  @param opencl_manager_in The manager that is running the opencl show
	 *  @param image_width_in The width of the input image for this operation
	 *  @param image_height_in The height of the input image for this operation
	 *  @param filter_size_in The size of the box filter to use (must be odd)
	 *  @throws std::runtime_error if there is an constructing this object (aka initting issue)
	 */
	BoxFilterBlur(OpenCLManager& opencl_manager_in, int image_width_in, int image_height_in, int filter_size_in);

	/** Destructor
	 */
	~BoxFilterBlur();

	/** Compute the operation. Note: this is non-blocking
	 *
	 *  @param input The input cv::Mat image to process
	 *  @throws std::runtime_error if there is an error running the kernel
	 */
	void compute(cv::Mat& input);

	/** Get the results of the operation
	 *
	 *  @param output The cv::Mat to fill with the data.  This cv::Mat must already be allocated.
	 *  @throws std::runtime_error if there is an error getting the data
	 */
	void getResults(cv::Mat& output);

	/** The a reference to the output buffer so we can move pipeline GPU operations
	 *
	 *  @returns Reference to the output buffer
	 */
	cl::Image2D& getOutputBuffer();

private:

	/** Constants
	 */
	static constexpr int LOCAL_WORK_GROUP_DIM_SIZE = 16;
	static constexpr int HORIZONTAL_KERNEL_LOCAL_WORK_GROUP_X = 1;
	static constexpr int HORIZONTAL_KERNEL_LOCAL_WORK_GROUP_Y = 128;
	static constexpr int VERTICAL_KERNEL_LOCAL_WORK_GROUP_X = 128;
	static constexpr int VERTICAL_KERNEL_LOCAL_WORK_GROUP_Y = 1;

	/** Init the Horizontal Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initHorizontalKernel();

	/** Init the Vertical Kernel
	 *
	 *  @throws std::runtime_error if there was an issue initting the kernel
	 */
	void initVerticalKernel();

	/** The opencl manager that is running the opencl show
	 */
	OpenCLManager& opencl_manager;

	/** The opencl Kernels
	 */
	OpenCLKernel horizontal_kernel;
	OpenCLKernel vertical_kernel;

	/** The height and width of the image
	 */
	int image_width{0};
	int image_height{0};

	/** The memory size of the image and the buffers
	 */
	int memory_size{0};

	/** The radius of the filer
	 */
	int filter_radius{0};

	/** The filter size that we will be using
	 */
	int filter_size{0};

	/** The buffers we will be using for the kernels
	 */
	cl::Image2D in_img_buffer_image2d;
	cl::Image2D horizontal_pass_out_img_buffer_image2d;
	cl::Image2D vertical_pass_out_img_buffer_image2d;
};

#endif // BOX_FILTER_BLUR_H
