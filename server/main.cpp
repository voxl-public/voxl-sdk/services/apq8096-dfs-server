/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *	this list of conditions and the following disclaimer in the documentation
 *	and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *	may be used to endorse or promote products derived from this software
 *	without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *	ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

// Project Includes
#include "constants.h"
#include "dfs_processing_engine.h"
#include "config_file.h"

// ModalAI includes
#include <modal_pipe_server.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>

// OpenCV Includes
#include <opencv2/core/mat.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/stereo.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>

// C/C++ Includes
#include <iostream>
#include <getopt.h>
#include <unistd.h>

// The command Line options
bool en_debug = false;
bool en_timing = false;
bool en_save_files = false;

// The DFS Processing Engine
std::shared_ptr<DFSProcessingEngine> dfs_processing_engine;

/** Prints the usage string (aka the help menu)
 */
static void _print_usage(void)
{
	std::cout << "voxl-dfs-server usually runs as a systemd background service. However, for debug" << std::endl;
	std::cout << "purposes it can be started from the command line manually with any of the following" << std::endl;
	std::cout << "debug options. When started from the command line, voxl-dfs-server will automatically" << std::endl;
	std::cout << "stop the background service so you don't have to stop it manually" << std::endl;
	std::cout << std::endl;
	std::cout << "-c, --config          load the config file only, for use by the config wizard" << std::endl;
	std::cout << "-d, --debug           run in debug mode which computes everything even when there" << std::endl;
	std::cout << "                        are no clients to receive the data. Prints how long each" << std::endl;
	std::cout << "                        computation step takes for performance monitoring" << std::endl;
	std::cout << "-h, --help            print this help message" << std::endl;
	std::cout << "-s, --save-files      save copies of disparity and target_overlay to /data/dfs/" << std::endl;
	std::cout << "                        Only use for debugging, not in normal operation." << std::endl;
	std::cout << "                        Enables debugging mode when activated." << std::endl;
	std::cout << "-t, --timing          enable timing mode for debug" << std::endl;
	std::cout << std::endl;
	return;
}

/** Parses out the
 *
 *  @param argc The number of arguments that exist
 *  @param argv The argument pointer
 *  @return true if the program should terminate
 */
static bool _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",				no_argument,	0, 'c'},
		{"debug",				no_argument,	0, 'd'},
		{"help",				no_argument,	0, 'h'},
		{"save-files",			no_argument,	0, 's'},
		{"timing",				no_argument,	0, 't'},
		{0, 0, 0, 0}
	};

	while (1)
	{
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdhst", long_options, &option_index);

		// Detect the end of the options.
		if (c == -1){
			break;
		}

		switch (c){
		case 0:
			// for long args without short equivalent that just set a flag nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_file_read();
			exit(0);
			break;

		case 'd':
			std::cout << "Enabling debug mode" << std::endl;
			en_debug = true;
			break;

		case 'h':
			_print_usage();
			return true;

		case 's':
			std::cout << "Enabling save files mode (and debug mode)" << std::endl;
			en_save_files = true;
			en_debug = true;
			break;

		case 't':
			en_timing = true;
			break;

		default:
			// Print the usage if there is an incorrect command line option
			_print_usage();
			return true;
		}
	}

	return false;
}

/** Terminates the app cleanly.
 *  Call this instead of return when it's time to exit to cleans up everything
 *
 *  @param ret The return code to use when exiting
 */
static void _quit(int ret)
{

	// Stop the dfs processing engine if it exists
	if (dfs_processing_engine)
	{
		dfs_processing_engine->stop();
	}

	// Close all the open pipe connections
	pipe_server_close_all();
	pipe_client_close_all();

	// Remove this process ID file from the filesystem so this app can run again latter
	remove_pid_file(PROCESS_NAME);

	// If we are exiting cleanly then say so
	if (ret == 0)
	{
		std::cout << "Exiting Cleanly" << std::endl;
	}

	// Exit with the return code
	exit(ret);
	return;
}


/**
 * @brief      return true if any of our 3 output pipes have clients
 */
static bool _has_clients(void)
{
	if(pipe_server_get_num_clients(DISPARITY_PIPE_CH)>0){
		return true;
	}
	if(pipe_server_get_num_clients(DISPARITY_SCALED_PIPE_CH)>0){
		return true;
	}
	if(pipe_server_get_num_clients(POINT_CLOUD_PIPE_CH)>0){
		return true;
	}
	return false;
}


/** Makes sure that this is the only instance of this app running.
 *  If any other instance is running, it is terminated and this instance
 *  takes over.  If this instance is unable to take over, this instance
 *  terminates.
 */
static void make_sure_process_is_unique(void)
{
	/* make sure another instance isn't running
	* if return value is -3 then a background process is running with
	* higher privileges and we couldn't kill it, in which case we should
	* not continue or there may be hardware conflicts. If it returned -4
	* then there was an invalid argument that needs to be fixed.
	*/
	if(kill_existing_process(PROCESS_NAME, 2.0) < -2){
		// Exit the app with an error code
		exit(-1);
	}

	// start signal handler so we can exit cleanly
	if(enable_signal_handler() == -1){
		std::cerr << "ERROR: failed to start signal handler" << std::endl;
		_quit(-1);
	}

	/* make PID file to indicate your project is running
	 * due to the check made on the call to rc_kill_existing_process() above
	 * we can be fairly confident there is no PID file already and we can
	 * make our own safely.
	 */
	make_pid_file(PROCESS_NAME);
	return;
}

/** Callback for whenever we connect to the camera server
 *
 * @param ch channel of the pipe (Unused)
 * @param context pipe context (Unused)
 */
static void _camera_connect_cb( __attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	std::cout << "Connected to camera server" << std::endl;
	return;
}

/** Callback for whenever we disconnect from the camera server
 *
 * @param ch channel of the pipe (Unused)
 * @param context pipe context (Unused)
 */
static void _camera_disconnect_cb( __attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	std::cout << "Disconnected from camera server" << std::endl;
	return;
}


/** Callback for whenever we connect to the camera server
 *
 * @param ch channel of the pipe (Unused)
 * @param meta The metadata for the camera frame
 * @param frame The camera frame data
 * @param context pipe context (Unused)
 */
static void _new_frame_handler(__attribute__((unused))int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	// Check to make sure the format of the frame is supported
	if (meta.format != IMAGE_FORMAT_STEREO_RAW8){
		std::cerr << "ERROR only support STEREO_RAW_8 images right now, got different format, skipping frame..." << std::endl;
		return;
	}


	// skip frame if there are no clients and not in debug mode
	if(!_has_clients() && !en_debug && !en_timing){
		// Stop the DFS engine if it is running since we have no subscribers to the data products
		if (dfs_processing_engine->is_started()){
			dfs_processing_engine->stop();
		}
		return;
	}

	// Start the DFS engine is it isnt already running
	if (!dfs_processing_engine->is_started()){
		dfs_processing_engine->start();
	}

	// skip frames if requested
	static int n_skipped = 0;
	if(n_skipped < skip_n_frames){
		if(en_debug){
			printf("skipping required frame\n");
		}
		n_skipped++;
		return;
	}

	// skip a frame if getting backed up
	if(pipe_client_bytes_in_pipe(ch)>0){
		n_skipped++;
		if(en_debug){
			fprintf(stderr, "WARNING, skipping frame on channel %d due to frame backup\n", ch);
		}
		return;
	}

	// Pass in the frame into the processing engine
	if(dfs_processing_engine->add_new_camera_data(meta, frame)){
		if(en_debug){
			fprintf(stderr, "WARNING, skipping frame on channel %d due to process queue backup\n", ch);
		}
		n_skipped++;
	}
	else{
		// great, all checks passed, now do something with the data!
		n_skipped = 0;
		if(en_debug){
			printf("starting new frame\n");
		}
	}

	return;
}


// static void _new_data_product_handler(DFSProcessingEngine::DataProducts& data_products)
// {
// 	camera_image_metadata_t disp_meta;

// 	disp_meta = data_products.meta;
// 	disp_meta.format = IMAGE_FORMAT_RAW8;
// 	disp_meta.height = data_products.disparity_scaled.rows;
// 	disp_meta.width = data_products.disparity_scaled.cols;
// 	disp_meta.size_bytes = disp_meta.height * disp_meta.width;
// 	disp_meta.stride = disp_meta.width;
// 	disp_meta.timestamp_ns = data_products.meta.timestamp_ns;
// 	pipe_server_send_camera_frame_to_channel(DISPARITY_SCALED_PIPE_CH, disp_meta, (char*)data_products.disparity_scaled.data);

// 	disp_meta = data_products.meta;
// 	disp_meta.format = IMAGE_FORMAT_RAW8;
// 	disp_meta.height = data_products.disparity_filtered.rows;
// 	disp_meta.width = data_products.disparity_filtered.cols;
// 	disp_meta.size_bytes = disp_meta.height * disp_meta.width;
// 	disp_meta.stride = disp_meta.width;
// 	disp_meta.timestamp_ns = data_products.meta.timestamp_ns;
// 	pipe_server_send_camera_frame_to_channel(DISPARITY_PIPE_CH, disp_meta, (char*)data_products.disparity_filtered.data);

// 	point_cloud_metadata_t pc_meta;
// 	pc_meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;
// 	pc_meta.timestamp_ns = data_products.meta.timestamp_ns;
// 	pc_meta.n_points = data_products.point_cloud_xyz.rows * data_products.point_cloud_xyz.cols;
// 	pc_meta.format = 0;
// 	pipe_server_send_point_cloud_to_channel(POINT_CLOUD_PIPE_CH, pc_meta, (float*)data_products.point_cloud_xyz.data);

// 	return;
// }



/** Setup the server pipes. This creates pipes for the data products that
 *  we will be creating in this app.  If a pipe cannot be created then
 *  this app is terminated
 */
static void setup_server_pipes(void)
{
	pipe_info_t info1 = { \
		DISPARITY_PIPE_NAME,		// name
		DISPARITY_PIPE_LOCATION,	// location
		"camera_image_metadata_t",	// type
		PROCESS_NAME,				// server_name
		(64*1024*1024),				// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(DISPARITY_PIPE_CH, info1, 0)){
		_quit(-1);
	}


	pipe_info_t info2 = { \
		DISPARITY_SCALED_PIPE_NAME,		// name
		DISPARITY_SCALED_PIPE_LOCATION,	// location
		"camera_image_metadata_t",		// type
		PROCESS_NAME,					// server_name
		(64*1024*1024),					// size_bytes
		0								// server_pid
	};

	if(pipe_server_create(DISPARITY_SCALED_PIPE_CH, info2, 0)){
		_quit(-1);
	}


	pipe_info_t info3 = { \
		POINT_CLOUD_PIPE_NAME,			// name
		POINT_CLOUD_PIPE_LOCATION,		// location
		"point_cloud_metadata_t",		// type
		PROCESS_NAME,					// server_name
		(64*1024*1024),					// size_bytes
		0								// server_pid
	};

	if(pipe_server_create(POINT_CLOUD_PIPE_CH, info3, 0)){
		_quit(-1);
	}

	return;
}


/** This connects to pipes that other applications are serving.
 *  This is so that we can get input data to process.  If the pipes
 *  cannot be connected to then the application terminates
 */
static void connect_client_pipes(void)
{
	std::cout << "waiting for camera server at: " << CAMERA_INPUT_NAME << std::endl;

	// Assign callbacks
	pipe_client_set_camera_helper_cb(0, _new_frame_handler, NULL);
	pipe_client_set_connect_cb(0, _camera_connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _camera_disconnect_cb, NULL);

	// Open connection to camera server in auto-reconnect mode
	int pipe_flags = EN_PIPE_CLIENT_CAMERA_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT;
	int ret = pipe_client_init_channel(0, (char*)CAMERA_INPUT_NAME, PROCESS_NAME, pipe_flags, 0);
	if (ret < 0){
		std::cerr << "ERROR: critical failure subscribing to pipe: " << CAMERA_INPUT_NAME << std::endl;
		pipe_print_error(ret);
		_quit(-1);
	}

	return;
}


int main(int argc, char *argv[])
{
	// Parse the command line options and terminate if the parser says we should terminate
	if (_parse_opts(argc, argv)){
		return -1;
	}

	// load and print config file
	if(config_file_read()){
		return -1;
	}
	config_file_print();

	// Set OpenCV to use only 1 thread! multithreading doesn't make anything
	// faster but can definitely waste more gpu and memory bandwidth.
	cv::setNumThreads(0);

	// Make sure this is the only instance of this app running, exits if not setup
	make_sure_process_is_unique();

	// Setup the server pipes, exits if not setup
	setup_server_pipes();

	/* indicate to the soon-to-be-started thread that we are initialized
	 * and running, this is an extern variable in start_stop.c
	 */
	main_running = 1;

	try{
		// Create the DFS Processing Engine
		dfs_processing_engine = std::shared_ptr<DFSProcessingEngine>(new DFSProcessingEngine(RAW_IMAGE_WIDTH, RAW_IMAGE_HEIGHT, QVGA_RESCALE_FACTOR, en_debug, en_timing, en_save_files, std::string(IMAGE_SAVE_DIR), n_disparity_levels));
		if (!dfs_processing_engine->init(INTRINSIC_CALIB_FILEPATH, EXTRINSIC_CALIB_FILEPATH))
		{
			_quit(-1);
		}
		//dfs_processing_engine->register_data_products_ready_callback(_new_data_product_handler);

	}
	catch (std::runtime_error& e){
		// Oh Boy!!! Something went very wrong!! :(
		std::cerr << e.what() << std::endl;
		_quit(-1);
	}

	// Start the processing
	dfs_processing_engine->start();

	// Connect to the client pipes
	connect_client_pipes();

	// Run the main loop forever!
	while (main_running){
		usleep(5000000);
	}

	// Stop the processing
	dfs_processing_engine->stop();

	// Close everything in a clean way
	_quit(0);
	return 0;
}